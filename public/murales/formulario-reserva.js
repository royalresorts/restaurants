(function () {
  $(".openForm").on("click", function (e) {
    e.preventDefault();
    $("#modalReserva").addClass("bg-active");
  });

  //   $("#modalReserva").addClass("bg-active");
  $("#modalReserva .modal-close").on("click", function (e) {
    $("#modalReserva").removeClass("bg-active");
  });

  $(".calerandate").caleran({
    minDate: new moment().add(1, "days"),
    singleDate: true,
    calendarCount: 1,
    format: "MM/DD/YYYY",
    autoCloseOnSelect: true,
  });
  $("#requestReservation").submit(function (e) {
    e.preventDefault();
    var elements = e.target.elements;
    if (elements.name.value == "") {
      elements.name.focus();
      $(".formData.Error").show();
      return;
    }
    if (elements.email.value == "") {
      elements.email.focus();
      $(elements.email).parent().addClass("error");
      //console.log($(elements.email).parent());
      $(".formData.Error").show();
      return;
    }
    $(".formData.Error").hide();

    //se muestra el loader antes de hacer la peticion
    $(".formData.loader").show();
    $(".formData.send").hide();
    // console.log(axios);

    var obj = {
      name: elements.name.value,
      email: elements.email.value,
      date: elements.date.value,
      pax: elements.pax.value,
      restaurant: elements.restaurant.value,
      EmailToSent: "restaurants@royalresorts.com",
    };
    console.log(obj);

    // $.post(
    //   "https://apidw.royalresorts.mobi/dtExtensions/8A22ADA9-C766-44AF-A316-F28062B52DC1",
    //   obj,
    //   function (data, status) {
    //     console.log(data);
    //     console.log(status);
    //   },
    //   "json"
    // );
    $.ajax({
      type: "POST",
      url: "https://apidw.royalresorts.mobi/dtExtensions/8A22ADA9-C766-44AF-A316-F28062B52DC1",
      data: JSON.stringify(obj),
      //data: obj,
      dataType: "json",
    })
      .done(function (data, textStatus, jqXHR) {
        if (console && console.log) {
          console.log("The request has been completed successfully");
          console.log(data);
          console.log(textStatus);
          if (data.errorCode == 0) {
            alert("The request has been completed successfully");
            elements.name.value = "";
            elements.name.email = "";
          } else {
            alert(data.errorDescription);
          }
          $(".formData.loader").hide();
          $(".formData.send").show();
        }
      })
      .fail(function (jqXHR, textStatus, errorThrown) {
        if (console && console.log) {
          console.log("La solicitud a fallado: " + textStatus);
          alert(textStatus);
          $(".formData.loader").hide();
          $(".formData.send").show();
        }
      });
  });
})(jQuery);
